package model;

import java.util.List;
import javax.swing.JPanel;
/**
 * interface for ask the panel of an attribute, or the set parameter.
 * @author Francesco
 *
 */
public interface AttibutePane {

    /**
     * ask for panel of one attribute,
     * an easy form.
     * @return the attribute panel
     */
    JPanel getPanel();

    /**
     * get the list of all attribute.
     * @return list of set parameter for one attribute.
     */
    List<String> getListAtr();

}
