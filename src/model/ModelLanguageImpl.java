package model;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import controller.ControlCore;

/**
 * help class for set language and country.
 * 
 * @author Francesco
 *
 */
public class ModelLanguageImpl implements ModelLanguage {

    private final List<String> listFile;
    private final List<String> listLanguage;
    private final List<String> listLangCountry;

    /**
     * constructor.
     * 
     * @param cc
     *            ControlCore
     */
    public ModelLanguageImpl(final ControlCore cc) {
        // "English en_US", su una sola riga considerata come una stringa, bisogna
        // separarla

        final String path = "/Conf/LanguageSelector.txt";

        // se si viene a creare una lista con elementi vuoti

        this.listFile = Collections.unmodifiableList(
                cc.getListRegex(path, true, "").stream().filter(s -> !s.equals("")).collect(Collectors.toList()));

        this.listLangCountry = Collections
                .unmodifiableList(this.listFile.stream().filter(s -> s.contains("_")).collect(Collectors.toList()));
        this.listLanguage = Collections
                .unmodifiableList(this.listFile.stream().filter(s -> !s.contains("_")).collect(Collectors.toList()));
        System.out.println(listLangCountry);
        System.out.println(listLanguage);
    }

    @Override
    public List<String> getListLanguage() {
        return Collections.unmodifiableList(this.listLanguage);
    }

    @Override
    public String getSelectedLangauge(final int selectedIndex) {
        // control, if the txt format is wrong.
        if (!this.listLangCountry.isEmpty()) {
            if (this.listLangCountry.size() > selectedIndex) {
                return this.listLangCountry.get(selectedIndex);
            }
        }
        // else return def, is default parameter
        return "def_def";
    }

}
