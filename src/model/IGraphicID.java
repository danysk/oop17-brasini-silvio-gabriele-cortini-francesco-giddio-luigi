package model;

import com.mxgraph.view.mxGraph;
/**
 * interface for create GraphicID object.
 * @author Luigi
 *
 */
public interface IGraphicID {

	/**
     * method to return the id of the GraphicID Object
     * @return id
     *            id of the object
     */ 
	public String getID();
	
	/**
     * method to set the id of the GraphicID Object
     * @param id
     *            id of the object
     */
	public void setID(String iD);
	
	/**
     * method to return the name of the GraphicID Object
     * @return name
     *            name of the object
     */ 
	public String getName();
	
	/**
     * method to set the name of the GraphicID Object
     * @param name
     *            name of the object
     */
	public void setName(String name);
	
	/**
     * method to create and return graphic representation of  GraphicID Object
     * @param graph 
     *            reference of view
     * @return Object
     *          Object view
     */
	public Object figreAtt(mxGraph graph);
	
}
