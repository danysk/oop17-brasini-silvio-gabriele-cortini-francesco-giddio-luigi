package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controller.ControlCore;
import model.QueryType;

/**
 * Window for perform a Select-From-Where interrogation.
 * 
 * @author silviobrasini
 *
 */
public class QueryInterrogation {

    private final JLabel labelSelect;
    private final JLabel labelFrom;
    private final JLabel labelWhere;
    private final JTextField jtfSelect;
    private final JTextField jtfFrom;
    private final JTextArea jtaWhere;
    private final JButton btnExe;
    private final JPanel panel;
    private final JPanel panelBody;
    private final JPanel panelBtn;
    private final JFrame frame;

    /**
     * Constructor of QueryInterrogation.
     * 
     * @param cc ControlCore
     */
    public QueryInterrogation(final ControlCore cc) {
        this.frame = new JFrame(cc.interpreterText("QueryInterrogation_Title"));
        cc.setGuiSize(this.frame, 2);
        this.panel = new JPanel(new BorderLayout());
        this.panelBody = new JPanel(new GridLayout(0, 2));
        this.panelBtn = new JPanel(new FlowLayout(FlowLayout.CENTER));
        this.labelSelect = new JLabel("SELECT");
        this.jtfSelect = new JTextField();
        this.labelFrom = new JLabel("FROM");
        this.jtfFrom = new JTextField();
        this.labelWhere = new JLabel("WHERE");
        this.jtaWhere = new JTextArea();
        this.btnExe = new JButton(cc.interpreterText("QueryInterrogation_Execute"));
        this.btnExe.addActionListener(e -> {
            final String error;
            final List<String> list = new ArrayList<>();
            list.add(this.jtfSelect.getText());
            list.add(this.jtfFrom.getText());
            list.add(this.jtaWhere.getText());
            error = cc.executeQuery(QueryType.INTERROGATION_QUERY, list);
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
        });
        this.panelBody.add(this.labelSelect);
        this.panelBody.add(this.jtfSelect);
        this.panelBody.add(this.labelFrom);
        this.panelBody.add(this.jtfFrom);
        this.panelBody.add(this.labelWhere);
        this.panelBody.add(this.jtaWhere);
        this.panelBtn.add(this.btnExe);
        this.panel.add(this.panelBody, BorderLayout.CENTER);
        this.panel.add(this.panelBtn, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.pack();
        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
    }
}
