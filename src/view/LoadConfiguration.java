package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.ControlCore;

/**
 * Frame which allow to load or delete a database configuration.
 * 
 * @author silviobrasini
 *
 */
public class LoadConfiguration {

    private static final int NAMEDB = 0;
    private static final int PATHDB = 1;
    private static final int USER = 2;
    private static final int PASSW = 3;
    private static final int TYPEDB = 4;
    private static final int DESCRIPTION = 5;
    private final JComboBox<String> jcb;
    private final JLabel label;
    private final JButton btnLoad;
    private final JButton btnDelete;
    private final JFrame frame;
    private final JPanel panel;
    private final JPanel panelBody;
    private final JPanel panelBtn;

    /**
     * Constructor of LoadConfiguration.
     * 
     * @param cc ControlCore
     */
    public LoadConfiguration(final ControlCore cc) {
        this.frame = new JFrame();
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setTitle(cc.interpreterText("LoadConf_Title"));
        cc.setGuiSize(this.frame, 1);
        this.panel = new JPanel(new BorderLayout());
        this.panelBody = new JPanel(new GridLayout(0, 2));
        this.panelBtn = new JPanel(new FlowLayout(FlowLayout.CENTER));
        this.label = new JLabel(cc.interpreterText("LoadConf_Label_Choose"));
        this.jcb = new JComboBox<>();
        this.loadJCB(cc);
        this.btnDelete = new JButton(cc.interpreterText("LoadConf_Btn_Delete"));
        this.btnDelete.addActionListener(e -> {
            cc.deleteElemCsv((String) this.jcb.getSelectedItem());
            this.loadJCB(cc);
        });
        this.btnLoad = new JButton(cc.interpreterText("LoadConf_Btn_Load"));
        this.btnLoad.addActionListener(e -> {
            if (this.jcb.getItemCount() == 0) {
                JOptionPane.showMessageDialog(null, cc.interpreterText("Empty_JCB"));
            } else {
                final String index = (String) this.jcb.getSelectedItem();
                final List<String> list = new ArrayList<>(cc.getDatabaseConfig(index));
                final Boolean conn = cc.setDbInfo(list.get(PATHDB), list.get(NAMEDB), list.get(USER), list.get(PASSW), list.get(TYPEDB), list.get(DESCRIPTION));
                if (conn) {
                    frame.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, cc.interpreterText("Missing_Lib"));
                }
            }
        });

        this.panelBody.add(this.label);
        this.panelBody.add(this.jcb);
        this.panelBtn.add(this.btnDelete);
        this.panelBtn.add(this.btnLoad);
        this.panel.add(this.panelBody, BorderLayout.CENTER);
        this.panel.add(this.panelBtn, BorderLayout.SOUTH);

        frame.getContentPane().add(this.panel);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Load the content of a JComboBox.
     * 
     * @param cc ControlCore
     */
    private void loadJCB(final ControlCore cc) {
        this.jcb.removeAllItems();
        final List<String> listDB = new ArrayList<>(cc.getDatabaseNameList());
        listDB.forEach(e -> this.jcb.addItem(e));
        if(cc.getErr() != null) {
            JOptionPane.showMessageDialog(null, cc.getErr().getMessage());
        }
    }

}
