package view;

import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.ControlCore;
import controller.ControlCoreImpl;

/**
 * Finestra iniziale.
 * 
 * @author Francesco
 *
 */
public class Main {

    private final JFrame frame = new JFrame();
    private final JButton setConn = new JButton();
    private final JButton dbManagement = new JButton();
    private final JButton setLanguage = new JButton();
    private final JButton btnLoad = new JButton();
    private final JButton btnLoadLibraries = new JButton();
    private Boolean closed;

    /**
     * Costruttore di Main.
     * 
     * @param cc
     *            ControlCore
     */
    public Main(final ControlCore cc) {
        cc.setGuiSize(this.frame, 2);
        final JPanel pane = new JPanel(new GridLayout(5, 1));
        this.reloadText(cc);

        this.btnLoadLibraries.addActionListener(e -> new LoadLibraries(cc));

        this.btnLoad.addActionListener(e -> new LoadConfiguration(cc));
        this.setConn.addActionListener(e -> {
            new ConnectToDatabase(cc);
        });
        this.dbManagement.addActionListener(e -> {
            new DbMainManager(cc);
        });
        this.setLanguage.addActionListener(e -> {
            new SelectLanguage(cc, this);
        });
        pane.add(this.btnLoadLibraries);
        pane.add(this.btnLoad);
        pane.add(this.setConn);
        pane.add(this.dbManagement);
        pane.add(this.setLanguage);
        this.frame.setContentPane(pane);
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setVisible(true);
        this.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(final WindowEvent e) {
                setClosed(true);
            }
        });
        setClosed(false);

    }

    void reloadText(final ControlCore cc) {
        // TODO Auto-generated method stub
        this.frame.setTitle(cc.interpreterText("Db_Editor"));
        this.setConn.setText(cc.interpreterText("SET_CONNECTION"));
        this.dbManagement.setText(cc.interpreterText("DB_MANAGEMENT"));
        this.setLanguage.setText(cc.interpreterText("SET_LANGUAGE"));
        this.btnLoad.setText(cc.interpreterText("LOAD_CONFIGURATION"));
        this.btnLoadLibraries.setText(cc.interpreterText("LOAD_LIBRARIES"));
    }

    /**
     * 
     * @return
     */
    public Boolean close() {
        if (this.frame.isVisible()) {
            this.frame.setVisible(false);
            this.frame.dispose();
            this.closed = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return
     */
    public Boolean isClosed() {
        return closed;
    }

    /**
     * 
     * @param closed
     */
    public void setClosed(final Boolean closed) {
        this.closed = closed;
    }
}
