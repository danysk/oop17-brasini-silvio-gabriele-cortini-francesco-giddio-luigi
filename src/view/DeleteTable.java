package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.ControlCore;
import model.QueryType;

/**
 * Window for delete a table.
 * 
 * @author silviobrasini
 *
 */
public class DeleteTable {

    private final JFrame frame;
    private final JLabel labelChooseTable;
    private final JComboBox<String> jcb;
    private final JButton btnDelete;
    private final JPanel panel;
    private final JPanel panelBody;
    private final JPanel panelBtn;

    /**
     * Constructor of DeleteTable.
     * 
     * @param cc ControlCore
     * 
     */
    public DeleteTable(final ControlCore cc) {
        this.frame = new JFrame();
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setTitle(cc.interpreterText("DeleteTable_Title"));
        this.panel = new JPanel(new BorderLayout());
        this.panelBody = new JPanel(new GridLayout(0, 2));
        this.panelBtn = new JPanel(new FlowLayout(FlowLayout.CENTER));

        this.labelChooseTable = new JLabel(cc.interpreterText("DeleteTable_Choose"));
        this.jcb = new JComboBox<>();
        this.btnDelete = new JButton(cc.interpreterText("DeleteTable_Delete"));
        this.loadComboBox(cc);
        this.btnDelete.addActionListener(e -> {
            final String error;
            final List<String> list = new LinkedList<>();
            list.add(cc.getTableList().get(this.jcb.getSelectedIndex()));
            error = cc.executeQuery(QueryType.DROP_TABLE, list);
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
            this.loadComboBox(cc);
        });

        this.panelBody.add(this.labelChooseTable);
        this.panelBody.add(this.jcb);
        this.panelBtn.add(this.btnDelete);
        this.panel.add(this.panelBody, BorderLayout.CENTER);
        this.panel.add(this.panelBtn, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.pack();
        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
    }

    /**
     * Load the content of a JComboBox.
     * 
     * @param cc
     */
    private void loadComboBox(final ControlCore cc) {
        this.jcb.removeAllItems();
        if (!cc.getTableList().isEmpty()) {
            cc.getTableList().forEach(this.jcb::addItem);
            this.jcb.setSelectedIndex(0);
        } else {
            this.btnDelete.setEnabled(false);
        }
    }

}
