package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;

/**
 * Window for set a connection with a database.
 * 
 * @author silviobrasini
 *
 */
public class ConnectToDatabase {


    private static final String FILE = ("/Conf/DatabaseTypes.txt");
    private final JFrame frame;
    private final JPanel panel;
    private final JPanel panelBody;
    private final JPanel panelOK;
    private final JButton btnOK;
    private final JButton btnPathDB;
    private final JTextField jtfPathDB;
    private final JTextField jtfUser;
    private final JTextField jtfPassw;
    private final JTextField jtfDBname;
    private final JTextField jtfDescription;
    private final JLabel labelNameDB;
    private final JLabel labelType;
    private final JLabel labelUser;
    private final JLabel labelPassw;
    private final JLabel labelPathDB;
    private final JLabel labelDescription;
    private final JComboBox<String> jcb;

    /**
     * Constructor of ConnectToDatabase.
     * 
     * @param cc
     *            ControlCore
     */
    public ConnectToDatabase(final ControlCore cc) {

        this.frame = new JFrame();
        cc.setGuiSize(this.frame, 2);
        final String title = cc.interpreterText("Connect_To_Database_Title");
        this.frame.setTitle(title);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.panel = new JPanel(new BorderLayout());
        this.panelBody = new JPanel(new GridLayout(0, 1));
        this.panelOK = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        this.labelDescription = new JLabel(cc.interpreterText("DB_Description"));
        this.jtfDescription = new JTextField();
        this.labelNameDB = new JLabel(cc.interpreterText("DB_Name"));
        this.jtfDBname = new JTextField();
        this.labelPathDB = new JLabel(cc.interpreterText("DB_Path"));
        this.jtfPathDB = new JTextField("localhost/");
        this.btnPathDB = new JButton(cc.interpreterText("DB_Path_Btn"));
        this.btnPathDB.addActionListener(new OpenFileChooser(this.jtfPathDB));
        this.labelType = new JLabel(cc.interpreterText("DB_Type"));
        this.labelUser = new JLabel(cc.interpreterText("DB_User"));
        this.jtfUser = new JTextField();
        this.labelPassw = new JLabel(cc.interpreterText("DB_Password"));
        this.jtfPassw = new JTextField();
        this.jcb = new JComboBox<>();
        this.loadComboBox(cc);
        this.panelBody.add(this.labelDescription);
        this.panelBody.add(this.jtfDescription);
        this.panelBody.add(this.labelNameDB);
        this.panelBody.add(this.jtfDBname);
        this.panelBody.add(this.labelPathDB);
        this.panelBody.add(this.jtfPathDB);
        this.panelBody.add(this.btnPathDB);
        this.panelBody.add(this.labelUser);
        this.panelBody.add(this.jtfUser);
        this.panelBody.add(this.labelPassw);
        this.panelBody.add(this.jtfPassw);
        this.panelBody.add(this.labelType);
        this.panelBody.add(this.jcb);
        this.btnOK = new JButton("OK");
        this.btnOK.addActionListener(e -> {
            final Boolean conn = cc.setDbInfo(this.jtfPathDB.getText(), this.jtfDBname.getText(),
                    this.jtfUser.getText(), this.jtfPassw.getText(), (String) this.jcb.getSelectedItem(),
                    this.jtfDescription.getText());
            if (conn) {
                this.frame.dispose();
            } else {
                JOptionPane.showMessageDialog(null, cc.interpreterText("DB_Error_Message"));
            }
        });
        this.panelOK.add(this.btnOK);
        this.panel.add(this.panelBody, BorderLayout.CENTER);
        this.panel.add(this.panelOK, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
    }

    /**
     * Load the elements in the combobox.
     */
    private void loadComboBox(final ControlCore cc) {
       
        final List<String> listFile = cc.getListRegex(FILE, true, "");
        listFile.forEach(this.jcb::addItem);
        if (!listFile.isEmpty()) {
            this.jcb.setSelectedIndex(0);
        }
    }

    /**
     * Private class which implements the file chooser.
     * 
     * @author silviobrasini
     *
     */
    private class OpenFileChooser implements ActionListener {

        private final  JTextField jtf;

        OpenFileChooser(final JTextField jtf) {
            this.jtf = jtf;
        }

        public void actionPerformed(final ActionEvent e) {
            final JFileChooser fileChooser = new JFileChooser();
            final int select = fileChooser.showOpenDialog(null);
            if (select == JFileChooser.APPROVE_OPTION) {
                final File file = fileChooser.getSelectedFile();
                this.jtf.setText(file.getAbsolutePath());
            }
        }
    }
}
