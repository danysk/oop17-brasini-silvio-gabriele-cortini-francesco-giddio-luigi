package controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import model.Database;

/**
 * Class for write and read the csv file containing the databases configurations.
 * 
 * @author silviobrasini
 *
 */
public class CsvReadWriteImpl implements CsvReadWrite {

    private static final String CSV_FILE_PATH = System.getProperty("user.home") + File.separator + "DatabaseConnections.csv";
    private static final String CSV_FILE_PATH_2 = System.getProperty("user.home") + File.separator + "DatabaseConnections2.csv";
    private static final int NAMEDB = 0;
    private static final int PATHDB = 1;
    private static final int USER = 2;
    private static final int PASSW = 3;
    private static final int TYPEDB = 4;
    private static final int DESCRIPTION = 5;
    private Exception err = null;

    @Override
    public void writeCsv(final Database db) {

        final File file = new File(CSV_FILE_PATH);
        final File fileTemp = new File(CSV_FILE_PATH_2);
        final Path path1 = Paths.get(CSV_FILE_PATH);
        final Path path2 = Paths.get(CSV_FILE_PATH_2);
        Boolean overwritten = false;

        this.checkExists();

        try {
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);
            final BufferedWriter writer = new BufferedWriter(new FileWriter(fileTemp, true));
            final CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);

            for (final CSVRecord record : parser) {
                if (!record.get(DESCRIPTION).equals(db.getDescription())) {
                    printer.printRecord(record.get(NAMEDB), record.get(PATHDB), record.get(USER), record.get(PASSW), record.get(TYPEDB), record.get(DESCRIPTION));
                } else {
                    printer.printRecord(db.getNameDB(), db.getPathDB(), db.getUser(), db.getPassw(), db.getTypeDB(), db.getDescription());
                    overwritten = true;
                }
            }

            if (!overwritten) {
                printer.printRecord(db.getNameDB(), db.getPathDB(), db.getUser(), db.getPassw(), db.getTypeDB(), db.getDescription());
            }

            reader.close();
            parser.close();
            writer.flush();
            writer.close();
            printer.close();
            Files.delete(path1);
            Files.move(path2, path1);

        } catch (Exception e) {
            setErr(e);
        }
    }

    @Override
    public List<String> readCSV() {

        final List<String> list = new ArrayList<>();
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(CSV_FILE_PATH));
            final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);
            parser.forEach(record -> list.add(record.get(DESCRIPTION)));
            parser.close();
            reader.close();
        } catch (Exception e) {
            setErr(e);
        }
        return list;
    }

    @Override
    public void deleteElement(final String elem) {
        final File file = new File(CSV_FILE_PATH);
        final File fileTemp = new File(CSV_FILE_PATH_2);
        final Path path1 = Paths.get(CSV_FILE_PATH);
        final Path path2 = Paths.get(CSV_FILE_PATH_2);

        try {
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);
            final BufferedWriter writer = new BufferedWriter(new FileWriter(fileTemp, true));
            final CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT);

            for (final CSVRecord record : parser) {
                if (!record.get(DESCRIPTION).equals(elem)) {
                    printer.printRecord(record.get(NAMEDB), record.get(PATHDB), record.get(USER), record.get(PASSW), record.get(TYPEDB), record.get(DESCRIPTION));
                }
            }

            reader.close();
            parser.close();
            writer.flush();
            writer.close();
            printer.close();
            Files.delete(path1);
            Files.move(path2, path1);

        } catch (Exception e) {
            this.setErr(e);
        }
    }

    @Override
    public List<String> getSelectedDatabase(final String dbDescription) {

        final List<String> list = new ArrayList<>();
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(CSV_FILE_PATH));
            final CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT);

            parser.forEach(record -> {
                if (record.get(DESCRIPTION).equals(dbDescription)) {
                    list.add(record.get(NAMEDB));
                    list.add(record.get(PATHDB));
                    list.add(record.get(USER));
                    list.add(record.get(PASSW));
                    list.add(record.get(TYPEDB));
                    list.add(record.get(DESCRIPTION));
                }
            });

            parser.close();
            reader.close();

        } catch (Exception e) {
            this.setErr(e);
        }
        return list;
    }

    @Override 
    public Exception getErr() {
        return err;
    }

    /**
     * Set the exception.
     * @param err
     */
    private void setErr(final Exception err) {
        this.err = err;
    }

    /**
     * Check if the csv file exists, if it doesn't exist than creates it.
     */
    private void checkExists() {
        final File file = new File(CSV_FILE_PATH);
        if (!file.exists()) {
            try {
                final BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.close();
            } catch (IOException e) {
                this.setErr(e);
            }
        }
    }
}
