package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


import interpreter.Interpreter;
import interpreter.InterpreterImpl;
import model.Database;
import model.DatabaseImpl;
import model.QueryType;

/**
 * where save select language, and get file txt as list.
 * 
 * @author Francesco
 *
 */
public class ControlCoreImpl implements ControlCore {

    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final String RES_PATH = System.getProperty("user.dir") + FILE_SEPARATOR + "res";
    private static final String LANGUAGE_PATH = "LanguagePack.MyLabels";
    private static final String DATABASE_PATH_RESURCE_BUNDLE = "DatabasePack.MyLabels";
    private Interpreter interpreterTr = new InterpreterImpl(LANGUAGE_PATH);
    private Interpreter interpreterSq = new InterpreterImpl(DATABASE_PATH_RESURCE_BUNDLE, "postgresql", "POSTGRESQL");
    private Database databaseInfo = new DatabaseImpl("", "", "", "", "", "");
    private final CsvReadWrite csvRW = new CsvReadWriteImpl();
    private Exception err;

    @Override
    public void setGuiSize(final JFrame frame, final int size) {
        final GUIsize gui = new GUIsizeImpl(frame);
        gui.setSize(size);
    }

    @Override
    public List<String> getDatabaseConfig(final String dbDescription) {
        return this.csvRW.getSelectedDatabase(dbDescription);
    }

    @Override
    public List<String> getDatabaseNameList() {
        this.setErr(this.csvRW.getErr());
        return this.csvRW.readCSV();
    }

    @Override
    public void deleteElemCsv(final String elem) {
        this.csvRW.deleteElement(elem);
    }

    @Override
    public List<String> getTableList() {
        final String query = getQueryCompose(QueryType.GET_TABLE_LIST, null);
        return this.databaseInfo.executeQueryListReturn(QueryType.GET_TABLE_LIST, query);
    }

    @Override
    public List<String> getColumnsList(final List<String> list) {
        // list è una lista con un solo argomento, ovvero la tabella scelta
        final String query = getQueryCompose(QueryType.GET_COLUMN_LIST, list);
        return this.databaseInfo.executeQueryListReturn(QueryType.GET_COLUMN_LIST, query);
    }

    @Override
    public List<String> getDatabaseList() {
        final String query = getQueryCompose(QueryType.GET_DATABASE_LIST, null);
        return this.databaseInfo.executeQueryListReturn(QueryType.GET_DATABASE_LIST, query);
    }

    @Override
    public void setLanguage(final String selectedLangauge) {
        // TODO Auto-generated method stub
        this.interpreterTr = new InterpreterImpl(LANGUAGE_PATH, selectedLangauge.split("_")[0],
                selectedLangauge.split("_")[1]);
    }

    @Override
    public Boolean setDbInfo(final String pathDB, final String nameDB, final String user, final String password,
            final String typeDB, final String description) {
        this.interpreterSq = new InterpreterImpl(DATABASE_PATH_RESURCE_BUNDLE, typeDB, typeDB.toUpperCase());
        this.databaseInfo = new DatabaseImpl(pathDB, nameDB, user, password, typeDB, description);
        this.csvRW.writeCsv(this.databaseInfo);
        return this.databaseInfo.tryConnection();
    }

    @Override
    public String interpreterText(final String string) {
        return this.interpreterTr.get(string);

    }

    @Override
    public String interpreterSql(final String text) {
        return interpreterSq.get(text);
    }

    @Override
    public String executeQuery(final QueryType queryType, final List<String> list) {
        final String query = this.getQueryCompose(queryType, list);
        return this.databaseInfo.executeQuery(queryType, query);
    }

    @Override
    public String getQueryCompose(final QueryType queryType, final List<String> list) {
        return this.interpreterSq.getQueryCompositor(queryType, list);
    }

    private List<String> getListSplitRegex(final List<String> l, final String regex) {
        // elimina qualsiasi carattere tranne "a-zA-Z" e "_"
        return Collections
                .unmodifiableList(l.stream().flatMap(s -> Stream.of(s.split(regex))).collect(Collectors.toList()));
    }

    @Override
    public String getResPath() {
        // TODO Auto-generated method stub
        return ControlCoreImpl.RES_PATH;
    }

    @Override
    public String getFileSep() {
        // TODO Auto-generated method stub
        return ControlCoreImpl.FILE_SEPARATOR;
    }

    private List<String> getListFromTxt(final String path) {
        List<String> listFile = new LinkedList<>();
        System.out.println(path);
        try {
            final InputStream in = ControlCoreImpl.class.getResourceAsStream(path);
            try (BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
                String line;
                while ((line = br.readLine()) != null) {
                    listFile.add(line);
                }
                br.close();
            }
            in.close();
            //listFile = Collections.unmodifiableList(
            //        Files.lines(Paths.get(ClassLoader.getSystemResource(path).toURI())).collect(Collectors.toList()));
        } catch (IOException e) {

            // TODO: handle exception
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        return listFile;
    }

    @Override
    public List<String> getListRegex(final String path, final boolean b, final String regex) {
        // TODO Auto-generated method stub
        List<String> list = this.getListFromTxt(path);
        if (b && regex.equals("")) {
            // if b==false but
            final String regex2 = "[^a-zA-Z0-9_]";
            list = this.getListSplitRegex(list, regex2);
        } else if (b) {
            list = this.getListSplitRegex(list, regex);
        }
        return list;
    }

    @Override
    public Exception getErr() {
        return err;
    }

    @Override
    public void setErr(final Exception err) {
        this.err = err;
    }

}