package viewFX;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.converter.IntegerStringConverter;
import model.AttributeImpl;
import model.QueryType;
import model.SqlDefault;

/**
 * 
 * @author ChristopherRoverelli
 *
 */
public class DBEditorController extends AnchorPane implements Initializable {

    // elenco componenti grafici divisi per TAB

    @FXML
    private TabPane tapMain;

    // region TabDatabaseManager

    @FXML
    private ComboBox<String> cmbDatabase;

    @FXML
    private TextField txfNewNameDB;

    @FXML
    private Button btnCreateDB;

    @FXML
    private Button btnAlterDB;

    @FXML
    private Button btnDropDB;

    @FXML
    private Button btnInfoDB;

    // endregion

    // region TabCreateTable

    @FXML
    private Tab tabCreateTable;

    @FXML
    private TableView<AttributeImpl> tbvCreateTable;

    @FXML
    private TableColumn<AttributeImpl, String> tbcName;

    @FXML
    private TableColumn<AttributeImpl, Boolean> tbcPrimaryKey;

    @FXML
    private TableColumn<AttributeImpl, String> tbcType;

    @FXML
    private TableColumn<AttributeImpl, Integer> tbcLenght;

    @FXML
    private TableColumn<AttributeImpl, Boolean> tbcNotNull;

    @FXML
    private TextField txfNameTable;

    @FXML
    private TextField txfNameAttr;

    @FXML
    private CheckBox ckbPrimaryKey;

    @FXML
    private ComboBox<String> cmbType;

    @FXML
    private TextField txfLenght;

    @FXML
    private CheckBox ckbNotNull;

    @FXML
    private Button btnAddRow;

    @FXML
    private Button btnDeleteRow;

    @FXML
    private Button btnCreateTable;

    // endregion

    // region TabAlterDropTable

    @FXML
    private Tab tabAlterDropTable;

    @FXML
    private ChoiceBox<String> chbTable;

    @FXML
    private Button btnDropTable;

    @FXML
    private Label lblTable;

    // endregion

    // region TabInsertTuple

    @FXML
    private Label lblTableName;

    @FXML
    private Button btnInsertTuple;

    @FXML
    private ComboBox<String> cmbTableInsertTuple;

    @FXML
    private VBox vbAttribute;

    // endregion

    // region TabDeleteTuple

    @FXML
    private Label lblTableName1;

    @FXML
    private Label lblAttr;

    @FXML
    private Label lblElAttr;

    @FXML
    private ComboBox<String> cmbTableDeleteTuple;

    @FXML
    private Button btnDeleteTuple;

    @FXML
    private ComboBox<String> cmbAttrDeleteTuple;

    @FXML
    private TextField txfAttrDeleteTuple;

    // endregion

    // region TabRelationManager

    @FXML
    private Label lblNameRelation;

    @FXML
    private Label lblTable1;

    @FXML
    private Label lblTable2;

    @FXML
    private Label lblAttrTable1;

    @FXML
    private Label lblCardTable1;

    @FXML
    private Label lblAttrTable2;

    @FXML
    private Label lblCardTable2;

    @FXML
    private Button btnAddTable1;

    @FXML
    private Button btnDeleteTable1;

    @FXML
    private Button btnAddTable2;

    @FXML
    private Button btnDeleteTable2;

    @FXML
    private Button btnSaveRelation;

    @FXML
    private TextField txfNameRelation;

    @FXML
    private TextArea txaCardTable1;

    @FXML
    private TextArea txaCardTable2;

    @FXML
    private ComboBox<String> cmbTable1;

    @FXML
    private ComboBox<String> cmbTable2;

    @FXML
    private ChoiceBox<String> chbAttrTable1;

    @FXML
    private ChoiceBox<String> chbAttrTable2;

    @FXML
    private TableView<String> tbvAttrTable1;

    @FXML
    private TableColumn<String, String> tbcAttrTable1;

    @FXML
    private TableView<String> tbvAttrTable2;

    @FXML
    private TableColumn<String, String> tbcAttrTable2;

    // endregion

    // region TabQueryManager

    @FXML
    private Tab tabQueryManager;

    @FXML
    private TextArea txaSELECT;

    @FXML
    private TextArea txaFROM;

    @FXML
    private TextArea txaFilters;

    @FXML
    private Label lblFilters;

    @FXML
    private Button btnExecuteQuery;

    // endregion

    // region TabFreeQuery

    @FXML
    private Tab tabFreeQuery;

    @FXML
    private CheckBox ckbFreeQuery;

    @FXML
    private TextArea txaFreeQuery;

    @FXML
    private Button btnExecuteFreeQuery;

    // endregion

    // lista per caricare gli attributi per la nuova tabelle
    private ObservableList<AttributeImpl> data = FXCollections.observableArrayList();
    private ObservableList<String> relTable1 = FXCollections.observableArrayList();
    private ObservableList<String> relTable2 = FXCollections.observableArrayList();
    // finestra per messaggi di errore
    Alert alert = new Alert(AlertType.ERROR);
    Alert msgQuery = new Alert(AlertType.INFORMATION);
    // variabile per il collegamento con l'applicazione principale
    private MainApp mainApp;

    /**
     * inizializzazione di alcuni controlli.
     * 
     * @param location
     *            url file FXML
     * @param resources
     *            ResourceBundle
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.tbcName.setMinWidth(USE_PREF_SIZE);
        this.tbcPrimaryKey.setMinWidth(USE_PREF_SIZE);
        this.tbcType.setMinWidth(USE_PREF_SIZE);
        this.tbcLenght.setMinWidth(USE_PREF_SIZE);
        this.tbcNotNull.setMinWidth(USE_PREF_SIZE);
        this.tbcAttrTable1.setMinWidth(USE_PREF_SIZE);
        this.tbcAttrTable2.setMinWidth(USE_PREF_SIZE);

        this.tbcName.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.tbcPrimaryKey.setCellValueFactory(new PropertyValueFactory<>("primaryKey"));
        this.tbcType.setCellValueFactory(new PropertyValueFactory<>("type"));
        this.tbcLenght.setCellValueFactory(new PropertyValueFactory<>("lenght"));
        this.tbcNotNull.setCellValueFactory(new PropertyValueFactory<>("notNull"));
        this.tbcAttrTable1.setCellValueFactory(new PropertyValueFactory<>("attribute1"));
        this.tbcAttrTable2.setCellValueFactory(new PropertyValueFactory<>("attribute2"));

        this.tbvCreateTable.setItems(data);
        this.tbvAttrTable1.setItems(relTable1);
        this.tbvAttrTable2.setItems(relTable2);

        this.txfLenght.setTextFormatter(new TextFormatter<>(new IntegerStringConverter()));
        this.txfLenght.setText("255");

        alert.setTitle("Error");
        alert.setHeaderText(null);
    }

    /**
     * collegamento con applicazione principale.
     * 
     * @param mainApp
     *            applicazione lancio UI
     */
    public void setApp(MainApp mainApp) {
        this.mainApp = mainApp;
        if (this.mainApp.getConnectDB()) {
            this.resetCHBDatabase();
            this.updateListTable();
        }
        final List<String> list = new LinkedList<>();
        final String FILE = (this.mainApp.getCC().getFileSep()+"Conf" + this.mainApp.getCC().getFileSep() + "Attribute.txt");
        for (final String string : this.mainApp.getCC().getListRegex(FILE, false, "")) {
            list.add(string.toUpperCase().replaceAll("#", ""));
        }
        this.cmbType.setItems(FXCollections.observableList(list));
        this.cmbType.getSelectionModel().select(0);
        this.msgQuery.setHeaderText(this.mainApp.getCC().interpreterText("msgQuery_Title"));
        this.enableLenght();

    }

    @FXML
    private void backType() {
        this.mainApp.typeEditor();
    }

    /**
     * chiusura finestra da menu File -> Close
     * 
     */
    @FXML
    private void closeWindow() {
        Platform.exit();
    }


    /**
     * settaggio finestra mesaggio d'errore
     * 
     * @param e
     *            eccezione per il recupero del messaggio d'errore
     */
    private void setMessageAlert(Exception e) {
        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        final String exceptionText = sw.toString();

        final Label label = new Label("The exception stacktrace was:");

        final TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        final GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
    }

    /**
     * creazione nuovo DB predendo il nome dal TextField apposito
     * 
     */
    @FXML
    private void createDB() {
        try {
            final List<String> list = new LinkedList<>();
            list.add(this.txfNewNameDB.getText());
            final String err = this.mainApp.getCC().executeQuery(QueryType.CREATE_DATABASE, list);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
            resetCHBDatabase();
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    /**
     * rinomina DB selezionato predendo il nome dal TextField apposito
     * 
     */
    @FXML
    private void alterDB() {
        try {
            final List<String> list = new LinkedList<>();
            list.add(this.cmbDatabase.getSelectionModel().getSelectedItem());
            list.add(this.txfNewNameDB.getText());
            final String err = this.mainApp.getCC().executeQuery(QueryType.ALTER_DATABASE, list);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
            resetCHBDatabase();
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    /**
     * elimina DB selezionato
     * 
     */
    @FXML
    private void dropDB() {
        try {
            final List<String> list = new LinkedList<>();
            list.add(this.cmbDatabase.getSelectionModel().getSelectedItem());
            final String err = this.mainApp.getCC().executeQuery(QueryType.DROP_DATABASE, list);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
            resetCHBDatabase();
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    @FXML
    private void infoDatabase() {
        try {
            Alert ff = new Alert(AlertType.INFORMATION);
            ff.setTitle("Information");
            ff.setHeaderText(null);
            ff.setContentText(this.mainApp.getCC().interpreterText("Message_Not"));
            ff.showAndWait();

        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }
    }

    /**
     * resetta la combobox da dove si seleziona il DB per la creazione, rinomina e
     * cancellazione. disabilita i pulsanti di rinomina e cancellazione nel caso non
     * ci sia un database disponibile
     * 
     */
    private void resetCHBDatabase() {
        this.cmbDatabase.getItems().clear();
        this.cmbDatabase.setItems(FXCollections.observableList(this.mainApp.getCC().getDatabaseList()));
        if (!this.cmbDatabase.getItems().isEmpty()) {
            this.cmbDatabase.getSelectionModel().select(0);
            this.btnAlterDB.setDisable(false);
            this.btnDropDB.setDisable(false);
        } else {
            this.btnAlterDB.setDisable(true);
            this.btnDropDB.setDisable(true);
        }
    }

    /**
     * nel caso in cui un attributo venga designato come Chiave Primaria allora
     * viene automaticamente designato come non nullo e disabilitato il controllo
     * per assegnarlo
     * 
     */
    @FXML
    private void changedPrimaryKey() {
        if (this.ckbPrimaryKey.isSelected()) {
            this.ckbNotNull.setSelected(true);
            this.ckbNotNull.setDisable(true);
        } else {
            this.ckbNotNull.setDisable(false);
        }
    }

    /*
     * abilita la textfield txfLenght  quando viene selezionato il tipo CHAR#
     */
    @FXML
    private void enableLenght() {
        final String FILE = (this.mainApp.getCC().getFileSep()+"Conf" + this.mainApp.getCC().getFileSep() + "Attribute.txt");
        final List<String> list = this.mainApp.getCC().getListRegex(FILE, false, "");
        this.txfLenght.setEditable(list.get(this.cmbType.getSelectionModel().getSelectedIndex()).contains("#"));
    }
    
    /**
     * aggiungi un attributo all'elenco di attributi per la nuova tabella
     * 
     */
    @FXML
    private void addAttrTable() {

        data.add(new AttributeImpl(this.txfNameAttr.getText(), this.ckbPrimaryKey.isSelected(),
                this.cmbType.getSelectionModel().getSelectedItem(), Integer.parseInt(this.txfLenght.getText()),
                this.ckbNotNull.isSelected()));

        this.txfNameAttr.clear();
        this.ckbPrimaryKey.setSelected(false);
        this.cmbType.getSelectionModel().select(0);
        this.txfLenght.setText("255");
        this.ckbNotNull.setSelected(false);
    }

    /**
     * elimina l'ultimo attributo aggiunto all'elenco di attributi per la nuova
     * tabella
     * 
     */
    @FXML
    private void deleteAttrTable() {
        if (!data.isEmpty()) {
            this.data.remove(this.data.size() - 1);
        }

    }

    /**
     * crea una nuova tabella
     * 
     */
    @FXML
    private void createTable() {
        try {
            final List<String> list = new LinkedList<>();
            list.add(this.txfNameTable.getText());
            getListAtr().forEach((item) -> {
                list.add(item);
            });
          final String err = this.mainApp.getCC().executeQuery(QueryType.CREATE_TABLE, list);
          if(err != null) {
              this.msgQuery.setContentText(err);
              this.msgQuery.showAndWait();
          }
            this.updateListTable();
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }
    }

    /**
     * aggiorna tutte le combobox e choicebox per la selezione delle tabelle
     * 
     */
    private void updateListTable() {
        if (!this.chbTable.getItems().isEmpty()) {
            this.chbTable.getItems().clear();
            this.cmbTable1.getItems().clear();
            this.cmbTable2.getItems().clear();
            this.cmbTableDeleteTuple.getItems().clear();
            this.cmbTableInsertTuple.getItems().clear();
        }

        if (this.mainApp.getConnectDB()) {
            this.chbTable.setItems(FXCollections.observableList(this.mainApp.getCC().getTableList()));
            this.chbTable.getSelectionModel().select(0);
            this.cmbTable1.setItems(FXCollections.observableList(this.mainApp.getCC().getTableList()));
            this.cmbTable1.getSelectionModel().select(0);
            this.cmbTable2.setItems(FXCollections.observableList(this.mainApp.getCC().getTableList()));
            this.cmbTable2.getSelectionModel().select(0);
            this.cmbTableDeleteTuple.setItems(FXCollections.observableList(this.mainApp.getCC().getTableList()));
            this.cmbTableDeleteTuple.getSelectionModel().select(0);
            this.cmbTableInsertTuple.setItems(FXCollections.observableList(this.mainApp.getCC().getTableList()));
            this.cmbTableInsertTuple.getSelectionModel().select(0);
            this.insertTupleTableChanged();
        }

    }

    /**
     * prepara la lista degli attributi per la creazione della tabella
     * 
     */
    private List<String> getListAtr() {
        // TODO Auto-generated method stub
        final List<String> list = new LinkedList<>();
        // replaceAll("\\s+", "") ->replaces 1 or more spaces.
        data.forEach((el) -> {
            list.add(el.getName().replaceAll("\\s+", ""));
            list.add(this.mainApp.getCC().interpreterSql(el.getType().toUpperCase().replaceAll("#", "")));
            if (el.getType().contains("#")) {
                list.add(this.mainApp.getCC().interpreterSql(SqlDefault.CREATE_TABLE_OPEN_BRACKET_TYPE.toString()));
                if ((el.getLenght() >= 0) && (el.getLenght() <= 255)) {
                    list.add(el.getLenght().toString());
                } else {
                    list.add("255");
                }

                list.add(this.mainApp.getCC().interpreterSql(SqlDefault.CREATE_TABLE_CLOSE_BRACKET_TYPE.toString()));
            }
            if (el.getPrimaryKey()) {
                list.add(this.mainApp.getCC().interpreterSql(SqlDefault.PRIMARY_KEY.toString()));
            }
            if (el.getNotNull()) {
                list.add(this.mainApp.getCC().interpreterSql(SqlDefault.NOT_NULL.toString()));
            }
            list.add(this.mainApp.getCC().interpreterSql(SqlDefault.SQL_COMMA.toString()));
        });

        return list;
    }

    /**
     * rinomina la tabella selezionata
     * 
     */
    @FXML
    private void alterTable() {
        try {
            /*
             * final List<String> list = new LinkedList<>();
             * list.add(this.mainApp.getCC().getTableList().get(this.chbTable.
             * getSelectionModel().getSelectedIndex()));
             * list.add(this.txfNewNameTable.getText());
             * this.mainApp.getCC().executeQuery(QueryType.ALTER_TABLE, list);
             * this.updateListTable();
             */
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }
    }

    /**
     * elimina la tabella selezionata
     * 
     */
    @FXML
    private void dropTable() {
        try {
            final List<String> list = new LinkedList<>();
            list.add(this.mainApp.getCC().getTableList().get(this.chbTable.getSelectionModel().getSelectedIndex()));
            final String err = this.mainApp.getCC().executeQuery(QueryType.DROP_TABLE, list);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
            this.updateListTable();
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    /**
     * prepara il conteiner con i controlli per l'inserimento di una nuova tupla
     * 
     */
    @FXML
    private void insertTupleTableChanged() {
        this.vbAttribute.getChildren().clear();
        List<String> list = new LinkedList<>();
        List<String> list1 = new LinkedList<>();
        list1.add(this.cmbTableInsertTuple.getSelectionModel().getSelectedItem());
        list.addAll(this.mainApp.getCC().getColumnsList(list1));
        list.forEach((item) -> {
            Label lblAt = new Label(item);
            TextField txfAt = new TextField();
            txfAt.setId(item);
            this.vbAttribute.getChildren().add(lblAt);
            this.vbAttribute.getChildren().add(txfAt);
        });
    }

    /**
     * inserisce una nuova tupla nella tabella selezionata
     * 
     */
    @FXML
    private void insertTuple() {
        try {
            final List<String> listTuples = new LinkedList<>();
            listTuples.add(this.cmbTableInsertTuple.getSelectionModel().getSelectedItem());
            this.vbAttribute.getChildren().forEach((item) -> {
                if (item.getId() != null) {
                    final TextField tmp = (TextField) item;
                    if (!tmp.getText().equals("")) {
                        listTuples.add(tmp.getId().toString());
                        listTuples.add(SqlDefault.SQL_COMMA.toString());
                    }
                }
            });
            listTuples.remove(listTuples.size() - 1);
            this.vbAttribute.getChildren().forEach((item) -> {
                if (item.getId() != null) {
                    final TextField tmp = (TextField) item;
                    if (!tmp.getText().equals("")) {
                        listTuples.add(tmp.getText().toString());
                        listTuples.add(SqlDefault.SQL_COMMA.toString());
                    }
                }
            });
            listTuples.remove(listTuples.size() - 1);
            final String err = this.mainApp.getCC().executeQuery(QueryType.INSERT_TUPLE, listTuples);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
            listTuples.clear();
            this.insertTupleTableChanged();
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }
    }

    /**
     * aggiorna la combobox per gli attributi in base alla tabella selezionata per
     * la cancellazione di una tupla
     * 
     */
    @FXML
    private void updateAttrDeleteTuple() {
        final List<String> list = new ArrayList<>();
        list.add(this.cmbTableDeleteTuple.getSelectionModel().getSelectedItem());
        this.cmbAttrDeleteTuple.setItems(FXCollections.observableList(this.mainApp.getCC().getColumnsList(list)));
        this.cmbAttrDeleteTuple.getSelectionModel().select(0);
    }

    /**
     * cancella una tupla in base al valore di un attributo
     * 
     */
    @FXML
    private void deleteTuple() {
        try {
            final List<String> list = new ArrayList<>();
            list.add(this.cmbTableDeleteTuple.getSelectionModel().getSelectedItem());
            list.add(this.cmbAttrDeleteTuple.getSelectionModel().getSelectedItem());
            list.add(this.txfAttrDeleteTuple.getText());
            final String err = this.mainApp.getCC().executeQuery(QueryType.DELETE_TUPLE, list);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }
    }

    /**
     * aggiorna la choicebox per gli attributi della prima tabella in base alla
     * tabella selezionata per creare una relazione tra due tabelle
     * 
     */
    @FXML
    private void updateAttrTable1CHB() {
        final List<String> list = new ArrayList<>();
        list.add(this.cmbTable1.getSelectionModel().getSelectedItem());
        this.chbAttrTable1.setItems(FXCollections.observableList(this.mainApp.getCC().getColumnsList(list)));
    }

    /**
     * aggiunge alla lista relTable1 un attributo della prima tabella per la
     * creazione di una relazione
     */
    @FXML
    private void addAttrTable1Relation() {
        this.relTable1.add(this.chbAttrTable1.getSelectionModel().getSelectedItem());
    }

    /**
     * aggiunge alla lista relTable2 un attributo della seconda tabella per la
     * creazione di una relazione
     */
    @FXML
    private void addAttrTable2Relation() {
        this.relTable2.add(this.chbAttrTable2.getSelectionModel().getSelectedItem());
    }

    /**
     * aggiorna la choicebox per gli attributi della seconda tabella in base alla
     * tabella selezionata per creare una relazione tra due tabelle
     * 
     */
    @FXML
    private void updateAttrTable2CHB() {
        final List<String> list = new ArrayList<>();
        list.add(this.cmbTable2.getSelectionModel().getSelectedItem());
        this.chbAttrTable2.setItems(FXCollections.observableList(this.mainApp.getCC().getColumnsList(list)));
    }

    /**
     * elimina dalla lista relTable1 l'ultimo attributo aggiunto
     */
    @FXML
    private void deleteAttrTable1Relation() {
        if (!relTable1.isEmpty()) {
            this.relTable1.remove(this.relTable1.size() - 1);
        }
    }

    /**
     * elimina dalla lista relTable2 l'ultimo attributo aggiunto
     */
    @FXML
    private void deleteAttrTable2Relation() {
        if (!relTable2.isEmpty()) {
            this.relTable2.remove(this.relTable2.size() - 1);
        }
    }
    
    //funzione da implementare in un futuro aggiornamento
    /**
     * salva una relazione tra due tabelle
     */
    @FXML
    private void saveRelation() {
        try {
            Alert ff = new Alert(AlertType.INFORMATION);
            ff.setTitle("Information");
            ff.setHeaderText(null);
            ff.setContentText(this.mainApp.getCC().interpreterText("Message_Not"));
            ff.showAndWait();

        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    /**
     * esegue un query di interogazione per il contenuto di una o più tabelle
     */
    @FXML
    private void executeQuery() {
        try {
            final List<String> list = new ArrayList<>();
            list.add(this.txaSELECT.getText());
            list.add(this.txaFROM.getText());
            list.add(this.txaFilters.getText());
            final String err = this.mainApp.getCC().executeQuery(QueryType.INTERROGATION_QUERY, list);
            if(err != null) {
                this.msgQuery.setContentText(err);
                this.msgQuery.showAndWait();
            }
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    /**
     * esegue una query qualsiasi inserita dall'utente
     */
    @FXML
    private void executeFreeQuery() {
        try {
            final List<String> list = new LinkedList<>();
            list.add(this.txaFreeQuery.getText());
            if (this.ckbFreeQuery.isSelected()) {
                final String err = this.mainApp.getCC().executeQuery(QueryType.FREE_QUERY_INTERROGATION, list);
                if(err != null) {
                    this.msgQuery.setContentText(err);
                    this.msgQuery.showAndWait();
                }
            } else {
                final String err = this.mainApp.getCC().executeQuery(QueryType.FREE_QUERY_UPDATE, list);
                if(err != null) {
                    this.msgQuery.setContentText(err);
                    this.msgQuery.showAndWait();
                }
            }
        } catch (Exception e) {
            this.setMessageAlert(e);
            alert.showAndWait();
        }

    }

    /**
     * richiama UI per la connessione al database e il caricamento delle sue
     * librerie
     */
    @FXML
    private void connectDatabase() {
        this.mainApp.connectDB();

    }

    /**
     * richiama UI per la selezione della lingua
     */
    @FXML
    private void selectLanguage() {
        this.mainApp.selectLang();
    }

}
