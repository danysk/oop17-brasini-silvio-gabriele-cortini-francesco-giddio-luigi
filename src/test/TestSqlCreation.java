package test;
/**
 * class for test postgres database.
 * @author Francesco
 *
 */

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import controller.ControlCore;
import controller.ControlCoreImpl;
import model.QueryType;

/**
 * 
 * @author Francesco
 *
 */
public class TestSqlCreation {

    /**
     * Create the database.
     */
    @Test
    public void createDatabase() {
        final ControlCore cc = new ControlCoreImpl();
        String createDatabase = "CREATE DATABASE PROVA4;";
        final ArrayList<String> list = new ArrayList<>();
        list.add(createDatabase);
        assertEquals(createDatabase, cc.getQueryCompose(QueryType.FREE_QUERY_INTERROGATION, list));
        list.clear();
        list.add("PROVA5");
        createDatabase = cc.getQueryCompose(QueryType.CREATE_DATABASE, list);
        assertEquals("CREATE DATABASE PROVA5;", createDatabase);
        list.clear();
    }

    /**
     * Alter the database.
     */
    @Test
    public void alterDatabase() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("PROVA4");
        list.add("PROVA5");
        string = cc.getQueryCompose(QueryType.ALTER_DATABASE, list);
        assertEquals("ALTER DATABASE PROVA4 RENAME TO PROVA5;", string);
    }

    /**
     * Drop the database.
     */
    @Test
    public void dropDatabase() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("PROVA4");
        string = cc.getQueryCompose(QueryType.DROP_DATABASE, list);
        assertEquals("DROP DATABASE IF EXISTS PROVA4;", string);
    }

    /**
     * Create the table.
     */
    @Test
    public void createTable() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("COMPANY");
        list.add("ID INT PRIMARY KEY NOT NULL,");
        list.add("NAME TEXT NOT NULL,");
        list.add("AGE INT NOT NULL,");
        list.add("ADDRESS CHAR(50)");
        string = cc.getQueryCompose(QueryType.CREATE_TABLE, list);
        final String string2 = "CREATE TABLE IF NOT EXISTS COMPANY("
                + "ID INT PRIMARY KEY NOT NULL,"
                + " NAME TEXT NOT NULL,"
                + " AGE INT NOT NULL," 
                + " ADDRESS CHAR(50) );";
        assertEquals(string2, string);
    }

    /**
     * Drop the table.
     */
    @Test
    public void dropTable() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("COMPANY");
        string = cc.getQueryCompose(QueryType.DROP_TABLE, list);
        assertEquals("DROP TABLE IF EXISTS COMPANY;", string);
    }

    /**
     * Insert tuple.
     * INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS) VALUES (1, 'Paul', 32, 'California');
     */
    @Test
    public void insertTuple() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("COMPANY");
        list.add("ID");
        list.add("NAME");
        list.add("AGE");
        list.add("ADDRESS");
        list.add("1");
        list.add("'Paul'");
        list.add("32");
        list.add("'California'");
        string = cc.getQueryCompose(QueryType.DROP_TABLE, list);
        assertEquals("DROP TABLE IF EXISTS COMPANY;", string);
    }

    /**
     * Delete tuple.
     */
    @Test
    public void deleteTuple() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("COMPANY");
        list.add("NAME");
        list.add("Carlo");
        string = cc.getQueryCompose(QueryType.DELETE_TUPLE, list);
        System.out.println(string);
        assertEquals("DELETE FROM COMPANY WHERE NAME='Carlo';", string);
    }

    /**
     * Interrogation Query.
     */
    @Test
    public void interrogationQuery() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("*");
        list.add("COMPANY");
        list.add("NAME='Carlo'");
        string = cc.getQueryCompose(QueryType.INTERROGATION_QUERY, list);
        assertEquals("SELECT * FROM COMPANY WHERE NAME='Carlo';", string);
    }

    /**
     * Free query interrogation.
     */
    @Test
    public void freeQueryInterrogation() {
        final ControlCore cc = new ControlCoreImpl();
        final ArrayList<String> list = new ArrayList<>();
        String string;
        list.add("COMPANY");
        string = cc.getQueryCompose(QueryType.FREE_QUERY_INTERROGATION, list);
        assertEquals(list.get(0), string);
    }
}
